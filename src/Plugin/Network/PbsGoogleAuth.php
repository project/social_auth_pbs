<?php

namespace Drupal\social_auth_pbs\Plugin\Network;

/**
 * Defines a Network Plugin for Social Auth PBS Google variant.
 *
 * @package Drupal\social_auth_pbs\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_pbs_google",
 *   short_name = "pbs_google",
 *   social_network = "PBS - Google",
 *   type = "social_auth",
 *   class_name = "\OpenPublicMedia\OAuth2\Client\Provider\Google",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_pbs\Settings\PbsAuthSettings",
 *       "config_id": "social_auth_pbs.settings"
 *     }
 *   }
 * )
 */
final class PbsGoogleAuth extends PbsNetworkBase {}
