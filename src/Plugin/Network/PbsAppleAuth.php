<?php

namespace Drupal\social_auth_pbs\Plugin\Network;

/**
 * Defines a Network Plugin for Social Auth PBS Apple variant.
 *
 * @package Drupal\social_auth_pbs\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_pbs_apple",
 *   short_name = "pbs_apple",
 *   social_network = "PBS - Apple",
 *   type = "social_auth",
 *   class_name = "\OpenPublicMedia\OAuth2\Client\Provider\Apple",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_pbs\Settings\PbsAuthSettings",
 *       "config_id": "social_auth_pbs.settings"
 *     }
 *   }
 * )
 */
final class PbsAppleAuth extends PbsNetworkBase {}
