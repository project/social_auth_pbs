<?php

namespace Drupal\social_auth_pbs\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\Plugin\Network\NetworkInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Social Auth PBS module routes.
 *
 * This controller handles callbacks for all variants of the PBS Account OAuth
 * implementation (Apple, Facebook, Google, etc.). Redirects and callbacks are
 * provided with the specific network based on a query parameter containing the
 * network short name.
 *
 * @see \Drupal\social_auth_pbs\Plugin\Network\PbsNetworkBase::getRedirectUrl()
 */
class PbsAuthController extends OAuth2ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function callback(NetworkInterface $network): RedirectResponse {
    $this->setSessionPrefix();
    $plugin_id = $this->dataHandler->get('plugin_id');
    try {
      /** @var \Drupal\social_auth\Plugin\Network\NetworkInterface $network */
      $network = $this->networkManager->createInstance($plugin_id);
    }
    catch (PluginException) {
      $this->messenger->addError($this->t('Log in failed, could not identify login provider. Please try again.'));
      $this->loggerFactory->get('social_auth_pbs')->error("Log in failed. Could not obtain sub-network ID from session.<br><br>Session ID: %id<br/>Session prefix: %prefix<br/>Session value: %value", [
        '%id' => $this->dataHandler->getSession()->getId(),
        '%prefix' => $this->dataHandler->getSessionPrefix(),
        '%value' => serialize($this->dataHandler->getSession()->all()),
      ]);
      return $this->redirect('user.login');
    }
    return parent::callback($network);
  }

  /**
   * {@inheritdoc}
   */
  public function redirectToProvider(NetworkInterface $network): Response {
    $request = $this->request->getCurrentRequest();
    $network_short_name = $request->query->get('network') ?? 'pbs';
    if ($network_short_name === 'pbs') {
      $plugin_id = 'social_auth_pbs';
    }
    else {
      $plugin_id = "social_auth_pbs_$network_short_name";
    }

    /** @var \Drupal\social_auth\Plugin\Network\NetworkInterface $network */
    $network = $this->networkManager->createInstance($plugin_id);
    $this->setSessionPrefix();
    $this->dataHandler->set('plugin_id', $network->getId());

    return parent::redirectToProvider($network);
  }

  /**
   * Sets the session prefix to the base provider ID.
   *
   * This controller is used for the all redirects and callbacks so the session
   * prefix must be shared between all variants to allow passing of the actual
   * network via the data handler.
   */
  private function setSessionPrefix(): void {
    $this->dataHandler->setSessionPrefix('social_auth_pbs');
  }

}
