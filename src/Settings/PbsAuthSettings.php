<?php

namespace Drupal\social_auth_pbs\Settings;

use Drupal\social_auth\Settings\SettingsBase;

/**
 * Defines methods to get Social Auth PBS settings.
 */
class PbsAuthSettings extends SettingsBase implements PbsAuthSettingsInterface {

  /**
   * The PBS application ID.
   *
   * @var string
   */
  protected ?string $applicationId = NULL;

  /**
   * The Hosted Login Customer ID.
   *
   * @var string
   */
  protected ?string $customerId = NULL;

  /**
   * The PBS Profile Services API base URL.
   *
   * @var string
   */
  protected ?string $profileServicesBaseUrl = NULL;

  /**
   * {@inheritdoc}
   */
  public function getApplicationId(): string {
    if (!$this->applicationId) {
      $this->applicationId = $this->config->get('application_id');
    }
    return $this->applicationId;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret(): ?string {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerId(): string {
    if (!$this->customerId) {
      $this->customerId = $this->config->get('customer_id');
    }
    return $this->customerId;
  }

  /**
   * {@inheritdoc}
   */
  public function getProfileServicesBaseUrl(): string {
    if (!$this->profileServicesBaseUrl) {
      $this->profileServicesBaseUrl = $this->config->get('profile_services_url');
    }
    return $this->profileServicesBaseUrl;
  }

}
