<?php

namespace Drupal\social_auth_pbs\Settings;

use Drupal\social_auth\Settings\SettingsInterface;

/**
 * Defines an interface for Social Auth PBS settings.
 */
interface PbsAuthSettingsInterface extends SettingsInterface {

  /**
   * Gets the PBS application ID.
   *
   * @return string
   *   PBS application ID.
   */
  public function getApplicationId(): string;

  /**
   * Gets the Hosted Login customer ID.
   *
   * @return string
   *   Hosted Login customer ID.
   */
  public function getCustomerId(): string;

  /**
   * Gets the PBS Profile Services API URL.
   *
   * @return string
   *   PBS Profile Services API URL.
   */
  public function getProfileServicesBaseUrl(): string;

}
