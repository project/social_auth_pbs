<?php

namespace Drupal\social_auth_pbs;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\social_auth\AuthManager\OAuth2Manager;
use Drupal\social_auth\User\SocialAuthUser;
use Drupal\social_auth\User\SocialAuthUserInterface;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Contains all the logic for PBS login integration.
 */
class PbsAuthManager extends OAuth2Manager {

  /**
   * Base URL of the Hosted Login service.
   */
  private string $hostedLoginBaseUrl = 'https://login.publicmediasignin.org';

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Used for accessing configuration object factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Used to get the authorization code from the callback request.
   */
  public function __construct(ConfigFactory $configFactory, LoggerChannelFactoryInterface $logger_factory, RequestStack $request_stack) {
    parent::__construct($configFactory->get('social_auth_pbs.settings'), $logger_factory, $this->request = $request_stack->getCurrentRequest());
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(): void {
    try {
      $this->setAccessToken($this->client->getAccessToken('authorization_code',
        ['code' => $this->request->query->get('code')]));
      // Finalize the login to link the account with PBS Profile Services API.
      $this->requestEndPoint(
          'POST',
          '/v2/login_resolve/ ',
          $this->settings->get('profile_services_url'),
          [
            'headers' => [
              'Application-Id' => $this->settings->get('application_id'),
            ],
          ],
        );
    }
    catch (IdentityProviderException $e) {
      $this->loggerFactory->get('social_auth_pbs')->error("There was an error during authentication. Exception: {$e->getMessage()}");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationUrl(): string {
    $options = [];
    $network = $this->request->query->get('network');

    // Set the `prompt` parameters to "create" to go directly to the register
    // form.
    if ($network == 'register') {
      $options['prompt'] = 'create';
    }

    // Returns the URL where user will be redirected.
    return $this->client->getAuthorizationUrl($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getPkceCode(): ?string {
    return $this->client->getPkceCode();
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): string {
    return $this->client->getState();
  }

  /**
   * {@inheritdoc}
   */
  public function getUserInfo(): ?SocialAuthUserInterface {
    if (!$this->user && $this->getAccessToken()) {
      /** @var \OpenPublicMedia\OAuth2\Client\Provider\PbsResourceOwner $owner */
      $owner = $this->client->getResourceOwner($this->getAccessToken());

      // Fetch additional profile details from PBS Profile Services API.
      $additional_data = $this->getExtraDetails();
      $additional_data['_profile'] = $this->requestEndPoint(
        'GET',
        '/v2/user/profile/',
        $this->settings->get('profile_services_url'),
        [
          'headers' => [
            'Application-Id' => $this->settings->get('application_id'),
          ],
        ],
      );

      $this->user = new SocialAuthUser(
        $owner->getName() ?? '',
        $owner->getId(),
        $this->getAccessToken(),
        $owner->getEmail(),
        additional_data: $additional_data
      );
      $this->user->setFirstName($owner->getFirstName());
      $this->user->setLastName($owner->getLastName());
    }
    return $this->user;
  }

  /**
   * {@inheritdoc}
   */
  public function requestEndPoint(string $method, string $path, ?string $domain = NULL, array $options = []): mixed {
    if (!$domain) {
      $domain = $this->hostedLoginBaseUrl;
    }
    // Add customer ID to Hosted Login URL requests.
    if ($domain == $this->hostedLoginBaseUrl) {
      $domain = $domain . '/' . $this->settings->get('customer_id');
    }
    $url = $domain . $path;
    $request = $this->client->getAuthenticatedRequest($method, $url, $this->getAccessToken(), $options);
    try {
      return $this->client->getParsedResponse($request);
    }
    catch (IdentityProviderException $e) {
      $this->loggerFactory->get('social_auth_pbs')->error("There was an error when requesting $url. Exception: {$e->getMessage()}");
    }
    return NULL;
  }

}
