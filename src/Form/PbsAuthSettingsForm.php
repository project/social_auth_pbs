<?php

namespace Drupal\social_auth_pbs\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\social_auth\Form\SocialAuthSettingsForm;
use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Settings form for Social Auth Google.
 */
class PbsAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_auth_pbs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return array_merge(
      parent::getEditableConfigNames(),
      ['social_auth_pbs.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NetworkInterface $network = NULL): array {
    /** @var \Drupal\social_auth\Plugin\Network\NetworkInterface $network */
    $network = $this->networkManager->createInstance('social_auth_pbs');
    $form = parent::buildForm($form, $form_state, $network);

    $config = $this->config('social_auth_pbs.settings');

    $form['network']['customer_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Customer ID'),
      '#default_value' => $config->get('customer_id'),
      '#description' => $this->t('The unique identifier for your Public Media SSO Hosted Login instance.'),
      '#weight' => 80,
    ];

    $form['network']['application_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Application ID'),
      '#default_value' => $config->get('application_id'),
      '#description' => $this->t('The unique identifier application ID for PBS profile services interactions.'),
      '#weight' => 81,
    ];

    $form['network']['profile_services_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('PBS Profiles Services URL'),
      '#default_value' => $config->get('profile_services_url'),
      '#description' => $this->t('Base URL to use for requests to the PBS Profile Services API.'),
      '#weight' => 82,
    ];

    $form['network']['authorized_redirect_url']['#weight'] = 90;
    $form['network']['advanced']['#weight'] = 100;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->config('social_auth_pbs.settings')
      ->set('application_id', trim($values['application_id']))
      ->set('client_id', trim($values['client_id']))
      ->set('customer_id', $values['customer_id'])
      ->set('endpoints', $values['endpoints'])
      ->set('profile_services_url', $values['profile_services_url'])
      ->set('scopes', $values['scopes'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
