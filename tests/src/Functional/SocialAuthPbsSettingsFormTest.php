<?php

namespace Drupal\Tests\social_auth_pbs\Functional;

use Drupal\Tests\social_auth\Functional\SocialAuthTestBase;

/**
 * Test Social Auth PBS settings form.
 *
 * @group social_auth
 *
 * @ingroup social_auth_pbs
 */
class SocialAuthPbsSettingsFormTest extends SocialAuthTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['social_auth_pbs'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->module = 'social_auth_pbs';
    $this->provider = 'pbs';
    $this->moduleType = 'social-auth';

    parent::setUp();
  }

  /**
   * Test if implementer is shown in the integration list.
   */
  public function testIsAvailableInIntegrationList() {
    $this->fields = [
      'application_id',
      'client_id',
      'customer_id',
      'profile_services_url',
    ];

    $this->checkIsAvailableInIntegrationList();
  }

  /**
   * Test if permissions are set correctly for settings page.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testPermissionForSettingsPage() {
    $this->checkPermissionForSettingsPage();
  }

  /**
   * Test settings form submission.
   */
  public function testSettingsFormSubmission() {
    $this->edit = [
      'application_id' => $this->randomString(10),
      'customer_id' => $this->randomString(10),
      'client_id' => $this->randomString(10),
      'profile_services_url' => $this->randomString(10),
    ];

    $this->checkSettingsFormSubmission();
  }

}
