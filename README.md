# Social Auth PBS

Social Auth PBS is a PBS authentication integration for
Drupal. It is based on the Social Auth and Social API projects

It adds to the site:

* A login URL: `/user/login/pbs`

* A settings form: `/admin/config/social-api/social-auth/pbs`.

* A PBS logo in the Social Auth Login block.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/social_auth_pbs).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/social_auth_pbs).


## Table of contents

- Requirements
- Installation
- Configuration
- How it works
- Support requests
- Maintainers


## Requirements

This module requires the following modules:

* [Social Auth](https://drupal.org/project/social_auth)
* [Social API](https://drupal.org/project/social_api)


## Installation

Install as you would normally install a contributed Drupal module. See
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
for more details.


## Configuration

1. Add your PBS project OAuth information in
   Configuration » User Authentication » PBS.
2. Place a Social Auth Login block in Structure » Block Layout.
3. If you already have a Social Auth Login block in the site, rebuild the cache.


## How it works

User can click on the PBS logo in the Social Auth Login block. You can also add
a button or link anywhere on the site that points to `/user/login/pbs`, so
theming and customizing the button or link is very flexible.

When the user opens the /user/login/pbs link, it automatically takes the user to
PBS for authentication. PBS then returns the user to Drupal site. If there is an
existing Drupal user with the same email address provided by PBS, that user is
logged in. Otherwise, a new Drupal user is created.

This module also supports login via the PBS provided Facebook and Google OAuth2
flows. These methods are only supported by URL access (no block buttons are
provided):

- **Facebook**: `social_auth_pbs.redirect_to_facebook`
   (/user/login/pbs-facebook)

- **Google**: `social_auth_pbs.redirect_to_google` (/user/login/pbs-google)
 
It is also possible to send users through a PBS Account creation flow by 
supplying a URL encoded version of the PBS login route to the `next` parameter 
of PBS's OAuth2 account creation form page. E.g. `[url]?next=[encoded_url]`.


## Support requests

* Before posting a support request, carefully read the installation
  instructions provided in module documentation page.

* Before posting a support request, check the Recent Log entries at
  admin/reports/dblog

* Once you have done this, you can post a support request at module issue
  queue: [https://www.drupal.org/project/issues/social_auth_pbs](https://www.drupal.org/project/issues/social_auth_pbs)

* When posting a support request, please inform if you were able to see any
  errors in the Recent Log entries.


## Maintainers

- Christopher C. Wells - [wells](https://www.drupal.org/u/wells)

* [Christopher C. Wells (wells)](https://www.drupal.org/u/wells)

Development sponsored by:

* [Cascade Public Media](https://www.drupal.org/cascade-public-media)
